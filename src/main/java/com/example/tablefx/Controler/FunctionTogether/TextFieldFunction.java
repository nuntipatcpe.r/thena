package com.example.tablefx.Controler.FunctionTogether;

import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;

public class TextFieldFunction {


//gun ************************************************************************
    public Boolean numberOnly(Integer maxSize, String oldValue, String newValue, TextField textField) {
        try {
            for (int tempI = 0 ; tempI < newValue.length() ; tempI++ ){
                char toChar = newValue.charAt(tempI);//แปลง ตัวละตัวที่รับเข้ามาของ newValue เป็น char
                int newValue2  = toChar;// แปลง toChar เป็น รหัส Ascii

                if ( newValue2 >= 48 && newValue2 <= 57){ // เชคว่าตัวที่รับเข้ามามัน เป็น 0-9 ป่าว ( เชคด้วยรหัส Ascii )
                    System.out.println( newValue );
                }
                else { // ถ้าไม่ใช่ 0-9 จะแสดงในช่อง textField จะไม่เซ็ทข้อความที่ผิดรูปแบบให้ จากนั้นจะบอกว่า "ต้องใส่เป็นตัวเลขหรือทศนิยมเท่านั้น"
                    System.out.println(" ต้องใส่เป็นตัวเลขหรือทศนิยมเท่านั้น");
                    textField.setText(oldValue);
                    return false;
                }

                int toInt = Integer.parseInt(textField.getText()); //แปลง String to int
                if( toInt > maxSize){
                    textField.setText(String.valueOf(maxSize));
                    return false;
                }
            }
        }catch (Exception e){

            System.out.println("ใส่ตัวเลขเท่านั้น");
            textField.setText( oldValue );
             return false;
        }

     return true;

    }
//gun ************************************************************************

//    boom ************************************************************************

//    checkLength ************************************************************************

    public Boolean checkLength(TextField text, Integer max,String oldValue){
        Boolean check = false;
        if(text.getLength()-1>=max){
            text.setText(oldValue);
            check = true;
        }
        return check;
    }
    public Boolean checkLength(TextArea text, Integer max,String oldValue){
        Boolean check = false;
        if(text.getLength()-1>=max){
            text.setText(oldValue);
            check = true;
        }
        return check;
    }
    //    checkLength ************************************************************************

//    errorMassage ************************************************************************
    public void addErrorMassage(VBox box ,Label label , String name){
        try {
            box.getChildren().add(errorStyleLabel(label,name));
        }catch (Exception e){

        }
    }
    public void removeErrorMassage(VBox box ,Label label){
            box.getChildren().remove(label);
    }
    public void addErrorMassage(HBox box ,Label label , String name){
        try {
            box.getChildren().add(errorStyleLabel(label,name));
        }catch (Exception e){

        }
    }
    public void removeErrorMassage(HBox box ,Label label){
            box.getChildren().remove(label);
    }
//    errorMassage ************************************************************************

//    Stylesheets ************************************************************************
    private Label errorStyleLabel(Label label, String nameError){
        label.setTextFill(Paint.valueOf("#DC3545"));
        label.setText(nameError);
        return label;
    }
//    Stylesheets ************************************************************************

//    Ascii ************************************************************************
    public Boolean checkAscii(Character start,Character end,TextField textField, String newValue, String oldValue) {
        boolean check = false;
        int newValueToAscii;
                    for(int charNewValue = 0 ;charNewValue <= newValue.length()-1 ; charNewValue++){
                        newValueToAscii =  newValue.charAt(charNewValue);
                         if(newValueToAscii >= (int)start && newValueToAscii<= (int)end){
                                check = true;
                        }else {
                             textField.setText(oldValue);
                             check = false;
                             break;
                         }
                    }
        return check;
    }
    public Boolean checkName(TextField textField, String newValue, String oldValue) {
        boolean check = false;
        try {
            int newValueToAscii;
            for(int charNewValue = 0 ;charNewValue <= newValue.length()-1 ; charNewValue++){
                newValueToAscii =  newValue.charAt(charNewValue);
                if(newValueToAscii >= '0' && newValueToAscii<='9' ||
                        newValueToAscii >= 'A' && newValueToAscii<='Z' ||
                        newValueToAscii >= 'a' && newValueToAscii<='z'||
                        newValueToAscii =='_'){
                    check = true;
                }else {
                    textField.setText(oldValue);
                    check = false;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("Test Field is Null ");
        }
        return check;
    }
//    Ascii ************************************************************************

//    boom ************************************************************************
}


