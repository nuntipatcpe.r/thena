package com.example.tablefx.Controler;

import com.example.tablefx.Application;
import com.example.tablefx.Controler.FunctionTogether.TextFieldFunction;
import com.example.tablefx.Model.ColumnType;
import com.example.tablefx.database.DAO.ColumnTypeDAO;
import com.example.tablefx.database.DAO.DAO;
import com.example.tablefx.database.DatabaseConnector;
import com.example.tablefx.database.type.DatabaseType;
import com.example.tablefx.database.type.MariaDB;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

public class TablePropertyControl implements Initializable {

TextFieldFunction textFieldFunction = new TextFieldFunction();

    @FXML
    private TextField nameTextField ;
    @FXML
    private TextField indexTextField;
    @FXML
    private TextField defaultTextField;
    @FXML
    private TextField descriptionTextField;
    @FXML
    private TextField sizeTextField;
    @FXML
    private TextField decimalTextField;
    @FXML
    private TextField validationTextTextField;
    @FXML
    private TextField regexTextField;

    @FXML
    private ComboBox<String> dataTypeComboBox;
    @FXML
    private ComboBox<String> keyComboBox;
    @FXML
    private ComboBox<String> inputMaskComboBox;
    @FXML
    private ComboBox<String> forMathComboBox;

    @FXML
    private Label tableNameLabel;
    @FXML
    private Label sizeSing_Label;



    @FXML
    private AnchorPane detailAnchorPane;


    @FXML
    private HBox descriptionHBox;
    Label descriptionErrorLabel = new Label();
    @FXML
    private HBox decimal_HBox;
    Label decimalAlert_Label = new Label();

    @FXML
    private HBox size_HBox;
    Label sizeAlert_Label = new Label();

    @FXML
    private HBox validationHBox;
    Label validationErrorLabel = new Label();

    @FXML
    private HBox nameHBox;
    Label nameErrorLabel = new Label();
    Label nameErrorMaxLabel = new Label();
    @FXML
    private HBox indexHBox;
    Label indexErrorLabel = new Label();
    @FXML
    private HBox defaultHBox;
    Label defaultErrorLabel = new Label();

    @FXML
    private HBox regexHBox;
    Label regexErrorLabel = new Label();



    @FXML
    private CheckBox notnullCheckBox;
    @FXML
    private CheckBox addAutoCheckBox;


    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private TableView<CreateTable> tableView;
    @FXML
    private TableColumn<CreateTable, String> nameATT ;
    @FXML
    private TableColumn<CreateTable, String> descriptionATT;
    @FXML
    private TableColumn<CreateTable, String> dataTypeATT;
    @FXML
    private TableColumn<CreateTable, String> keyATT;
    @FXML
    private TableColumn<CreateTable, String> indexATT;
    @FXML
    private TableColumn<CreateTable, String> sizeATT;
    @FXML
    private TableColumn<CreateTable, String> nullATT;
    @FXML
    private TableColumn<CreateTable, String> addAutoATT;
    @FXML
    private TableColumn<CreateTable, String> inputMaskATT;
    @FXML
    private TableColumn<CreateTable, String> forMathATT;
    @FXML
    private TableColumn<CreateTable, String> decimalATT;
    @FXML
    private TableColumn<CreateTable, String> defaultATT;
    @FXML
    private TableColumn<CreateTable, String> validationATT;
    @FXML
    private TableColumn<CreateTable, String> regexATT;

    @FXML
    private Label decimalSing_Label;

    ObservableList<CreateTable> listTableFx = FXCollections.observableArrayList();
    Integer row = null;

    DAO columnTypeDAO;
    DatabaseConnector databaseConnector;
    String nameDatabase;
    String beforeChengPkString ;
    String shortText = "Short Text";
    String longText = "Long Text";
    String number = "Number";
    String largeNumber = "Large Number";
    String dateOrTime = "Date/Time";
    String currency = "Currency";
    String autoNumber = "AutoNumber";
    String yesOrNo = "Yes/No";
    String primary = "Primary Key";
    String foreign = "Foreign Key";
    String password = "password";
    String inputMask_1 = "00-0000-0000";
    String forMath_1 = "dd/mmmm/yyyy";
    String forMath_2 = "dd.mmmm.yyyy";
    String forMath_3 = "THB";
    String forMath_4 = "USD";
    String choose = "เลือก";

    public void lodeTopicFormTableTopic(String name){
        this.nameDatabase = name;
        tableNameLabel.setText("กำหนดคุณสมบัติของโครงสร้างข้อมูล- "+name);
       // refresh();

    }

    public void backToTableNameButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(Application.class.getResource("table-topic-view.fxml")));
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow(); //สร้าง stage ใช้stage ตัว ที่ link
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    private void addAllDatabase(){

    }
//boolean havePk = false;

    public void onNextButtonClick(ActionEvent event) {
        columnTypeDAO  = new ColumnTypeDAO(databaseConnector);
        List<ColumnType> listDatabase = columnTypeDAO.getAll();

        StringBuilder errorString = new StringBuilder();
        List<ColumnType> havePK = new ArrayList<>();
        List<ColumnType> savePK = new ArrayList<>();

            for(CreateTable table: listTableFx){
                ColumnType columnType = new ColumnType();
                columnType.setName(table.nameATT);
                columnType.setDescription(table.descriptionATT);
                columnType.setDataType(table.dataTypeATT);
                columnType.setDefault_value(table.defaultATT);
                columnType.setKey_name(table.keyATT);
                columnType.setIndex_number(table.indexATT);
                columnType.setSize(table.sizeATT);
                columnType.setNot_null(table.nullATT);
                columnType.setFormat(table.forMathATT);
                columnType.setAdd_auto(table.addAutoATT);
                columnType.setInput_mask(table.inputMaskATT);
                columnType.setDecimal_number(table.decimalATT);
                columnType.setRegex(table.regexATT);
                columnType.setValidation_text(table.validationATT);
                columnType.setEntity(nameDatabase);
                savePK.add(columnType);
                for(ColumnType columnTypeOld : listDatabase){ //check Pk กับ Column อื่นๆ
                    if(columnTypeOld.getName().equals(table.nameATT) && columnTypeOld.getEntity().equals(nameDatabase) ){
                        havePK.add(columnType);
                        errorString.append(" "+columnTypeOld.getName());
                    }
                }

            }

            if(havePK.size() == 0 ){
                for(ColumnType columnType: savePK){
                    columnTypeDAO.save(columnType);
                    System.out.println("save"+columnType.getName());
                }
                listTableFx.clear();
            }else{
                 Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                 alert.setTitle("มี PK ซ้ำ");
                 alert.setContentText("คุณต้องการ update Column ->>"+errorString+" <<- หรือไม่ ");
                 alert.setHeaderText(null);
                 alert.showAndWait().ifPresent(response -> {
                     if (response == ButtonType.OK) {
                         savePK.removeAll(havePK);
                         for(ColumnType columnType: havePK){
                             System.out.println("update "+columnType.getName());
                             columnTypeDAO.update(columnType);
                         }
                         for(ColumnType columnType1: savePK){
                             System.out.printf("save "+columnType1.getName());
                             columnTypeDAO.save(columnType1);
                         }
                         listTableFx.clear();
                }});
            }

        }

//    }
    public void onCancelButtonClick(ActionEvent event) {
        clearText();
        tableView.setDisable(false);
        detailAnchorPane.setDisable(true);
    }

    //_______________________Edit______________________________
    public void onAddDataButtonClick(ActionEvent event) {
        deleteButton.setVisible(false);
        editButton.setVisible(false);
        tableView.setDisable(true);
        detailAnchorPane.setDisable(false);
        nameTextField.setDisable(false);
        row = null;
    }
    public void onEditTableDataButtonClick (ActionEvent event){ //แก้ไขตารา

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Edit table");
            alert.setContentText("คุณต้องการ Edit Table หรือไม่ ");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    try {
                        addDataTableToEdit();
                        beforeChengPkString = listTableFx.get(row).nameATT;
                        tableView.setDisable(true);
                        editButton.setVisible(false);
                        deleteButton.setVisible(false);
                        detailAnchorPane.setDisable(false);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

    }

    public void onDeleteDataButtonClick(ActionEvent event){
       // addDataTableToEdit();
        if(row != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("ยืนยัน");
            alert.setContentText("ยืนยันการลบ");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    try {
//                        tableView.getItems().removeAll(tableView.getSelectionModel().getSelectedItem());
                        //        columnTypeDAO  = new ColumnTypeDAO(databaseConnector);

                        //columnTypeDAO.delete(setDataToColumnType());
                       // refresh();
                        tableView.getItems().removeAll(tableView.getSelectionModel().getSelectedItem());
                        detailAnchorPane.setDisable(true);
                        deleteButton.setVisible(false);
                        editButton.setVisible(false);
                        row = null;
                    }catch (Exception e){
                    }
                }
            });

        }

    }
    //_______________________Edit______________________________

    //_______________________save_______________________

    public void onSaveDataButtonClick(ActionEvent event) {
            if (row == null ) { //กรณีไม่กด Edit Table
                try {
                    listTableFx.add(listTableFx.size()+1,new CreateTable(nameTextField.getText(),descriptionTextField.getText(),dataTypeComboBox.getValue(),keyComboBox.getValue(),indexTextField.getText(),sizeTextField.getText(),""+notnullCheckBox.isSelected(),""+addAutoCheckBox.isSelected(),inputMaskComboBox.getValue(),forMathComboBox.getValue(),decimalTextField.getText(),defaultTextField.getText(),validationTextTextField.getText(),regexTextField.getText()));
                }catch (Exception e){
                    listTableFx.add(new CreateTable(nameTextField.getText(),descriptionTextField.getText(),dataTypeComboBox.getValue(),keyComboBox.getValue(),indexTextField.getText(),sizeTextField.getText(),""+notnullCheckBox.isSelected(),""+addAutoCheckBox.isSelected(),inputMaskComboBox.getValue(),forMathComboBox.getValue(),decimalTextField.getText(),defaultTextField.getText(),validationTextTextField.getText(),regexTextField.getText()));
                }
            } else {
                listTableFx.add(row, new CreateTable(nameTextField.getText(),descriptionTextField.getText(),dataTypeComboBox.getValue(),keyComboBox.getValue(),indexTextField.getText(),sizeTextField.getText(), ""+notnullCheckBox.isSelected(),""+addAutoCheckBox.isSelected(),inputMaskComboBox.getValue(),forMathComboBox.getValue(),decimalTextField.getText(),defaultTextField.getText(),validationTextTextField.getText(),regexTextField.getText()));
                listTableFx.remove(row+1);
                deleteButton.setVisible(false);
                editButton.setVisible(false);
                row = null;
            }

        clearText();
        detailAnchorPane.setDisable(true);
        tableView.setDisable(false);
        }

//    private  void testSave(){
//
//            boolean editPk = true;
//        List<ColumnType> list = columnTypeDAO.getAll();
//        for(ColumnType columnType : list){ //check Pk กับ Column อื่นๆ
//            if(columnType.getName().equals(nameTextField.getText()) && columnType.getTable_name().equals(nameDatabase) ){
//                System.out.println("ซ้ำ");
//                editPk = false;
//                break;
//            }
//
//        }
//
//            if (row == null ) { //กรณีไม่กด Edit Table
//
//                if(editPk){
//                    columnTypeDAO.save(setDataToColumnType());
//                    refresh();
//                    clearText();
//                    detailAnchorPane.setDisable(true);
//                    tableView.setDisable(false);
//                }else {
//                    Alert alert = new Alert(Alert.AlertType.ERROR);
//                    alert.setTitle("Not Edit Pk");
//                    alert.setContentText("มี PK ซ้ำกับ Column อื่น");
//                    alert.setHeaderText(null);
//                    nameTextField.setText(beforeChengPkString);
//                    alert.show();
//                }
//
//            } else if (!beforeChengPkString.equals(nameTextField.getText())) {
//
//                if(editPk){ //check จะEdit จิงๆ รึป่าว
//                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                    alert.setTitle("Edit Pk");
//                    alert.setContentText("คุณต้องการ Edit Pk หรือไม่ ");
//                    alert.setHeaderText(null);
//                    alert.showAndWait().ifPresent(response -> {
//                        if (response == ButtonType.OK) {
//                            ColumnType columnType1 = new ColumnType();
//                            columnType1.setName(beforeChengPkString);
//                            columnType1.setTable_name(nameDatabase);
//                            columnTypeDAO.save(setDataToColumnType());
//                            columnTypeDAO.delete(columnType1);
//
//                            refresh();
//                            clearText();
//                            detailAnchorPane.setDisable(true);
//                            tableView.setDisable(false);
//
//                            refresh();
//                        }
//                    });
//                }else{
//                    Alert alert = new Alert(Alert.AlertType.ERROR);
//                    alert.setTitle("Not Edit Pk");
//                    alert.setContentText("มี PK ซ้ำกับ Column อื่น");
//                    alert.setHeaderText(null);
//                    nameTextField.setText(beforeChengPkString);
//                    alert.show();
//
//                }
//
//            } else {
//
//
//                columnTypeDAO.update(setDataToColumnType());
//                refresh();
//                clearText();
//                detailAnchorPane.setDisable(true);
//                tableView.setDisable(false);
//
//            }
//
//
//
//
//
//
//        }
//        private void testEdit(){
//             addDataTableToEdit();
//            if(row != null){
//                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                alert.setTitle("ยืนยัน");
//                alert.setContentText("ยืนยันการลบ");
//                alert.setHeaderText(null);
//                alert.showAndWait().ifPresent(response -> {
//                    if (response == ButtonType.OK) {
//                        try {
//
//                            columnTypeDAO  = new ColumnTypeDAO(databaseConnector);
//                            columnTypeDAO.delete(setDataToColumnType());
//                             refresh();
//
//                            detailAnchorPane.setDisable(true);
//                            deleteButton.setVisible(false);
//                            editButton.setVisible(false);
//                            row = null;
//                        }catch (Exception e){
//                        }
//                    }
//                });
//
//            }
//        }
    public void onCancelPropertyButtonClick(ActionEvent event) {
        if(!detailAnchorPane.disabledProperty().get()){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("ยืนยัน");
            alert.setContentText("ต้องการยกเลิก");
            alert.setHeaderText(null);
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    try {
                        if(row != null){
                            deleteButton.setVisible(false);
                            editButton.setVisible(false);
                            row = null;
                            detailAnchorPane.setDisable(true);
                            tableView.setDisable(false);
                        }
                        clearText();

                    }catch (Exception e){

                    }
                }
            });

        }

    }
    //_______________________save_______________________


    //_________________Row_______________________
    public void checkRowTable(MouseEvent mouseEvent) {
        try {
            TablePosition tablePosition =  tableView.getSelectionModel().getSelectedCells().get(0);
            row = tablePosition.getRow();
            deleteButton.setVisible(true);
            editButton.setVisible(true);
        }catch (Exception e){

        }
    }
    //_________________Row_______________________
    public void chekDataTypeComboBox(ActionEvent event) {

        String item = dataTypeComboBox.getSelectionModel().getSelectedItem();
        if(item.equals(dateOrTime)||item.equals(yesOrNo)||item.equals(currency)||item.equals(autoNumber)||item.equals(shortText)||item.equals(longText)){
            decimalTextField.setVisible(false);
            decimalAlert_Label.setVisible(false);
            decimalSing_Label.setVisible(false);
        }else{
            decimalTextField.setVisible(true);
            if(!decimalAlert_Label.getText().equals("")){
                decimalAlert_Label.setText("");
            }
            decimalSing_Label.setVisible(true);
            decimalAlert_Label.setVisible(true);
        }

        if(item.equals(longText) || item.equals(largeNumber)|| item.equals(dateOrTime)|| item.equals(currency)||item.equals(yesOrNo)){
            sizeTextField.setVisible(false);
            sizeAlert_Label.setVisible(false);
            sizeSing_Label.setVisible(false);
        }
        else {
            sizeAlert_Label.setVisible(true);
            sizeTextField.setVisible(true);
            sizeSing_Label.setVisible(true);
            if(!sizeAlert_Label.getText().equals("")){
                sizeAlert_Label.setText("");

            }
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        textFieldFunction.addErrorMassage(nameHBox,new Label(),"PK");
        //to connect DataBase
        try {
            DatabaseType mariaDB = new MariaDB(); // obj driver
            mariaDB.getDriverClassName();
            databaseConnector = new DatabaseConnector(mariaDB,"localhost","3306"
                ,"thena","design_view","1234");
            databaseConnector.getDatabaseConnection(); // บอกให้เชื่อมต่อ

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        //**/

        deleteButton.setVisible(false);
        editButton.setVisible(false);
        detailAnchorPane.setDisable(true);
        addColumnTable();
        addDataComboBox();
        boxMarginStyle();
        //*************************************gun************************************************************
        this.decimalTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed( ObservableValue<? extends String> observableValue, String oldValue, String newValue ){

                if(!textFieldFunction.numberOnly(16,oldValue,newValue,decimalTextField)){
                    TablePropertyControl.this.textFieldFunction.addErrorMassage(decimal_HBox, decimalAlert_Label,"ต้องใส่เป็นตัวเลข และความยาวไม่เกิน 16 ตัวอักษร");
                }else {
                    TablePropertyControl.this.textFieldFunction.removeErrorMassage(decimal_HBox, decimalAlert_Label);
                }

            }
        });
        this.sizeTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if(!textFieldFunction.numberOnly( 50 ,oldValue,newValue,sizeTextField )){
                    TablePropertyControl.this.textFieldFunction.addErrorMassage(size_HBox, sizeAlert_Label,"ต้องใส่เป็นตัวเลข และความยาวไม่เกิน 50 ตัวอักษร");
                }else {
                    TablePropertyControl.this.textFieldFunction.removeErrorMassage(size_HBox, sizeAlert_Label);
                }

            }
        });
        this.indexTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if( textFieldFunction.checkAscii('0','9',indexTextField , newValue,oldValue)){
                    textFieldFunction.removeErrorMassage(indexHBox, indexErrorLabel);
                }else{
                    textFieldFunction.addErrorMassage(indexHBox, indexErrorLabel,"Can only put 0-9");

                }
            }
        });
        this.defaultTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if(textFieldFunction.checkLength(defaultTextField,50,oldValue)){
                    TablePropertyControl.this.textFieldFunction.addErrorMassage(defaultHBox, defaultErrorLabel,"Max size "+ defaultTextField.getLength());
                }else{
                    TablePropertyControl.this.textFieldFunction.removeErrorMassage(defaultHBox, defaultErrorLabel);
                }

            }
        });
        this.validationTextTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if(textFieldFunction.checkLength(validationTextTextField, 20, oldValue)){
                    textFieldFunction.addErrorMassage(validationHBox, validationErrorLabel,"Max size "+ validationTextTextField.getLength());
                }else{
                    textFieldFunction.removeErrorMassage(validationHBox, validationErrorLabel);
                }
            }
        });
        this.regexTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if(textFieldFunction.checkLength(regexTextField, 10, oldValue)){
                    textFieldFunction.addErrorMassage(regexHBox, regexErrorLabel,"Max size "+ regexTextField.getLength());
                }else{
                    textFieldFunction.removeErrorMassage(regexHBox, regexErrorLabel);
                }

            }
        });
        //*************************************gun************************************************************
        this.nameTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {


                if(textFieldFunction.checkName(nameTextField,newValue,oldValue)){
                    textFieldFunction.removeErrorMassage(nameHBox,nameErrorLabel);
                }else {
                    int newValueToAscii;
                    try {
                        for(int charNewValue = 0 ;charNewValue <= newValue.length()-1 ; charNewValue++){
                            newValueToAscii =  newValue.charAt(charNewValue);
                            textFieldFunction.addErrorMassage(nameHBox,nameErrorLabel,"/"+(char)newValueToAscii+"/  ->Can only put A-Z,a-z,0-9,_");
                        }
                    }catch (Exception e){
                        System.out.println("Test Field is Null ");
                    }



                }

                if(textFieldFunction.checkLength(nameTextField,16,oldValue)){
                    textFieldFunction.addErrorMassage(nameHBox,nameErrorMaxLabel,"Max size "+ nameTextField.getLength());
                }else {
                    textFieldFunction.removeErrorMassage(nameHBox,nameErrorMaxLabel);
                }
            }



        });
        this.descriptionTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {
                if(textFieldFunction.checkLength(descriptionTextField,20,oldValue)){
                    textFieldFunction.addErrorMassage(descriptionHBox,descriptionErrorLabel,"Max size"+ descriptionTextField.getLength());
                }else{
                    textFieldFunction.removeErrorMassage(descriptionHBox,descriptionErrorLabel);
                }
            }
        });
    }

    private void addDataTableToEdit() {
        nameTextField.setText(listTableFx.get(row).getNameATT());
        indexTextField.setText(listTableFx.get(row).getIndexATT());
        defaultTextField.setText(listTableFx.get(row).getDefaultATT());
        regexTextField.setText(listTableFx.get(row).getRegexATT());
        descriptionTextField.setText(listTableFx.get(row).getDescriptionATT());
        sizeTextField.setText(listTableFx.get(row).getSizeATT());
        decimalTextField.setText(listTableFx.get(row).getDecimalATT());
        validationTextTextField.setText(listTableFx.get(row).getValidationATT());
        dataTypeComboBox.setValue(listTableFx.get(row).getDataTypeATT());
        keyComboBox.setValue(listTableFx.get(row).getKeyATT());
        inputMaskComboBox.setValue(listTableFx.get(row).getInputMaskATT());
        forMathComboBox.setValue(listTableFx.get(row).getDescriptionATT());
        notnullCheckBox.setSelected(Boolean.parseBoolean(listTableFx.get(row).getNullATT()));
        addAutoCheckBox.setSelected(Boolean.parseBoolean(listTableFx.get(row).addAutoATT));
    }
    private void addDataComboBox(){

        List<String> dataType = new ArrayList<>();
        dataType.add(shortText);
        dataType.add(longText);
        dataType.add(number);
        dataType.add(largeNumber);
        dataType.add(dateOrTime);
        dataType.add(currency);
        dataType.add(autoNumber);
        dataType.add(yesOrNo);
        dataType.add(choose);

        List<String> key = new ArrayList<>();
        key.add(primary);
        key.add(foreign);
        key.add(choose);

        List<String> inputMask = new ArrayList<>();
        inputMask.add(password);
        inputMask.add(inputMask_1);
        inputMask.add(choose);
        List<String> forMath = new ArrayList<>();
        forMath.add(forMath_1);
        forMath.add(forMath_2);
        forMath.add(forMath_3);
        forMath.add(forMath_4);
        forMath.add(choose);

        dataTypeComboBox.setItems(
                FXCollections.observableList(dataType)
        );
        keyComboBox.setItems(
                FXCollections.observableList(key)
        );
        inputMaskComboBox.setItems(
                FXCollections.observableList(inputMask)
        );
        forMathComboBox.setItems(
                FXCollections.observableList(forMath)
        );
        dataTypeComboBox.setValue(choose);
        keyComboBox.setValue(choose);
        inputMaskComboBox.setValue(choose);
        forMathComboBox.setValue(choose);

    }
    private void addColumnTable(){
        nameATT.setCellValueFactory(new PropertyValueFactory<>("nameATT"));
        dataTypeATT.setCellValueFactory(new PropertyValueFactory<>("dataTypeATT"));
        descriptionATT.setCellValueFactory(new PropertyValueFactory<>("descriptionATT"));
        dataTypeATT.setCellValueFactory(new PropertyValueFactory<>("dataTypeATT"));
        keyATT.setCellValueFactory(new PropertyValueFactory<>("keyATT"));
        indexATT.setCellValueFactory(new PropertyValueFactory<>("indexATT"));
        nullATT.setCellValueFactory(new PropertyValueFactory<>("nullATT"));
        dataTypeATT.setCellValueFactory(new PropertyValueFactory<>("dataTypeATT"));
        addAutoATT.setCellValueFactory(new PropertyValueFactory<>("addAutoATT"));
        inputMaskATT.setCellValueFactory(new PropertyValueFactory<>("inputMaskATT"));
        forMathATT.setCellValueFactory(new PropertyValueFactory<>("forMathATT"));
        decimalATT.setCellValueFactory(new PropertyValueFactory<>("decimalATT"));
        defaultATT.setCellValueFactory(new PropertyValueFactory<>("defaultATT"));
        validationATT.setCellValueFactory(new PropertyValueFactory<>("validationATT"));
        regexATT.setCellValueFactory(new PropertyValueFactory<>("regexATT"));
        sizeATT.setCellValueFactory(new PropertyValueFactory<>("sizeATT"));
        tableView.setItems(listTableFx);
    }
    private void boxMarginStyle(){
        defaultErrorLabel.setPadding(new Insets(0, 0, 0, 15));
        nameErrorLabel.setPadding(new Insets(0, 0, 0, 15));
        nameErrorMaxLabel.setPadding(new Insets(0, 0, 0, 15));
        indexErrorLabel.setPadding(new Insets(0, 0, 0, 15));
        defaultErrorLabel.setPadding(new Insets(0, 0, 0, 15));
        sizeAlert_Label.setPadding(new Insets(0, 0, 0, 15));
        decimalAlert_Label.setPadding(new Insets(0, 0, 0, 15));
        regexErrorLabel.setPadding(new Insets(0, 0, 0, 15));
//        keyHBoxLabel.setPadding(new Insets(top,right,bottom,left));
    }
    private void clearText(){
        dataTypeComboBox.setValue(choose);
        keyComboBox.setValue(choose);
        inputMaskComboBox.setValue(choose);
        forMathComboBox.setValue(choose);

        nameTextField.clear();
        indexTextField.clear();
        defaultTextField.clear();
        regexTextField.clear();
        descriptionTextField.clear();
        sizeTextField.clear();
        decimalTextField.clear();
        validationTextTextField.clear();

        notnullCheckBox.setSelected(false);
        addAutoCheckBox.setSelected(false);

        descriptionErrorLabel.setText("");
        decimalAlert_Label.setText("");
        sizeAlert_Label.setText("");
        validationErrorLabel.setText("");
        nameErrorLabel.setText("");
        nameErrorMaxLabel.setText("");
        indexErrorLabel.setText("");
        defaultErrorLabel.setText("");
        regexErrorLabel.setText("");
    }

//   ____________________Database_______________________
    private void  refresh(){
        listTableFx.clear();
        columnTypeDAO  = new ColumnTypeDAO(databaseConnector);
        List<ColumnType> columnTypeList = columnTypeDAO.getAll();
        for (ColumnType columnType : columnTypeList ) {
            if(columnType.getEntity().equals(nameDatabase)){
                listTableFx.add(new CreateTable(columnType.getName(),columnType.getDescription(),columnType.getDataType(),columnType.getKey_name(),columnType.getIndex_number(),columnType.getSize(),columnType.getNot_null(),columnType.getAdd_auto(),columnType.getInput_mask(),columnType.getFormat(),columnType.getDecimal_number(),columnType.getDefault_value(),columnType.getValidation_text(),columnType.getRegex()));
            }
        }
        clearText();
    }

    private void addDataToTable(){



    }

    private ColumnType setDataToColumnType(){
        ColumnType columnType1 = new ColumnType();
        columnType1.setName(nameTextField.getText());
        columnType1.setDescription(descriptionTextField.getText());
        columnType1.setDataType(String.valueOf(dataTypeComboBox.getValue())); // ปัญหา
        columnType1.setDefault_value(defaultTextField.getText());
        columnType1.setKey_name(keyComboBox.getValue());
        columnType1.setIndex_number(indexTextField.getText());
        columnType1.setSize(sizeTextField.getText());
        columnType1.setNot_null(String.valueOf(notnullCheckBox.isSelected()));
        columnType1.setFormat(forMathComboBox.getValue());
        columnType1.setAdd_auto(String.valueOf(addAutoCheckBox.isSelected()));
        columnType1.setInput_mask(inputMaskComboBox.getValue());
        columnType1.setDecimal_number(decimalTextField.getText());
        columnType1.setRegex(regexTextField.getText());
        columnType1.setValidation_text(validationTextTextField.getText());
        columnType1.setEntity(nameDatabase);
        return columnType1;
    }
//    ____________________Database_______________________
}
