package com.example.tablefx.Controler;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class CreateTable {

    String nameATT ,descriptionATT,dataTypeATT,keyATT,indexATT,sizeATT,inputMaskATT,forMathATT,decimalATT,defaultATT,validationATT,regexATT;
    String nullATT ,addAutoATT ;


    public CreateTable(String nameATT, String descriptionATT, String dataTypeATT, String keyATT, String indexATT, String sizeATT , String nullATT, String addAutoATT , String inputMaskATT, String forMathATT, String decimalATT, String defaultATT, String validationATT , String regexATT) {
        this.nameATT = nameATT;
        this.descriptionATT = descriptionATT;
        this.dataTypeATT = dataTypeATT;
        this.keyATT = keyATT;
        this.indexATT = indexATT;
        this.sizeATT = sizeATT;
        this.inputMaskATT = inputMaskATT;
        this.forMathATT = forMathATT;
        this.decimalATT = decimalATT;
        this.defaultATT = defaultATT;
        this.validationATT = validationATT;
        this.regexATT = regexATT;
        this.nullATT = nullATT;
        this.addAutoATT = addAutoATT;
    }


    public String getNameATT() {
        return nameATT;
    }

    public void setNameATT(String nameATT) {
        this.nameATT = nameATT;
    }

    public String getDescriptionATT() {
        return descriptionATT;
    }

    public void setDescriptionATT(String descriptionATT) {
        this.descriptionATT = descriptionATT;
    }

    public String getDataTypeATT() {
        return dataTypeATT;
    }

    public void setDataTypeATT(String dataTypeATT) {
        this.dataTypeATT = dataTypeATT;
    }

    public String getKeyATT() {
        return keyATT;
    }

    public void setKeyATT(String keyATT) {
        this.keyATT = keyATT;
    }

    public String getIndexATT() {
        return indexATT;
    }

    public void setIndexATT(String indexATT) {
        this.indexATT = indexATT;
    }

    public String getSizeATT() {
        return sizeATT;
    }

    public void setSizeATT(String sizeATT) {
        this.sizeATT = sizeATT;
    }

    public String getInputMaskATT() {
        return inputMaskATT;
    }

    public void setInputMaskATT(String inputMaskATT) {
        this.inputMaskATT = inputMaskATT;
    }

    public String getForMathATT() {
        return forMathATT;
    }

    public void setForMathATT(String forMathATT) {
        this.forMathATT = forMathATT;
    }

    public String getDecimalATT() {
        return decimalATT;
    }

    public void setDecimalATT(String decimalATT) {
        this.decimalATT = decimalATT;
    }

    public String getDefaultATT() {
        return defaultATT;
    }

    public void setDefaultATT(String defaultATT) {
        this.defaultATT = defaultATT;
    }

    public String getValidationATT() {
        return validationATT;
    }

    public void setValidationATT(String validationATT) {
        this.validationATT = validationATT;
    }

    public String getRegexATT() {
        return regexATT;
    }

    public void setRegexATT(String regexATT) {
        this.regexATT = regexATT;
    }

    public String getNullATT() {
        return nullATT;
    }

    public void setNullATT(String nullATT) {
        this.nullATT = nullATT;
    }

    public String getAddAutoATT() {
        return addAutoATT;
    }

    public void setAddAutoATT(String addAutoATT) {
        this.addAutoATT = addAutoATT;
    }
}
