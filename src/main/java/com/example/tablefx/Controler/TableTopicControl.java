package com.example.tablefx.Controler;

import com.example.tablefx.Application;
import com.example.tablefx.Controler.FunctionTogether.TextFieldFunction;
import com.example.tablefx.Model.ColumnType;
import com.example.tablefx.Model.Topic;
import com.example.tablefx.database.DAO.TopicDAO;
import com.example.tablefx.database.DatabaseConnector;
import com.example.tablefx.database.type.DatabaseType;
import com.example.tablefx.database.type.MariaDB;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class TableTopicControl implements Initializable {
    TextFieldFunction textFieldFunction = new TextFieldFunction();

    @FXML
    private TextField tableNameTextField;

    @FXML
    private TextArea descriptionTextArea;

    @FXML
    private VBox messageErrorVbox;

    @FXML
    private VBox errorTextAreaVbox;

    Label errorNameTable = new Label();
    Label errorMaxTable = new Label();
    Label errorDescriptionNameTable = new Label();

    boolean checkPk = false;
    public void nextToCrateTableButton(ActionEvent event) {

        if(tableNameTextField.getText().equals("")) {
            try {
                FXMLLoader lode = new FXMLLoader(Application.class.getResource("alert-box-view.fxml"));
                Parent root = lode.load();

                AlertBoxControl alertBox = lode.getController();
                alertBox.setStageAlertBox("ชื่อ");
                Stage stageAlertBox = new Stage();
                stageAlertBox.initModality(Modality.APPLICATION_MODAL);
                stageAlertBox.setResizable(false);

                Scene scene = new Scene(root);
                stageAlertBox.setTitle("กรองข้อมูลใหม่");
                stageAlertBox.setScene(scene);
                stageAlertBox.show();
            }catch (Exception e){
                e.printStackTrace();
            }

        }else{

                Topic topicTable = new Topic();
                topicTable.setName(tableNameTextField.getText());
                topicTable.setDescription(descriptionTextArea.getText());
                TopicDAO topicTableDao = new TopicDAO(databaseConnector);
                List<Topic> topicList = topicTableDao.getAll();

                for(Topic topic : topicList ){
                    if(topic.getName().equals(tableNameTextField.getText().toLowerCase())){
                        System.out.println("____"+tableNameTextField.getText());
                        checkPk =true;
                        break;
                    }else {
                        checkPk = false;
                    }
                }


                if(checkPk){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("มี PK ซ้ำ");
                    alert.setContentText("คุณต้องการ update Column ->>"+tableNameTextField.getText()+" <<- หรือไม่ ");
                    alert.setHeaderText(null);
                    alert.showAndWait().ifPresent(response -> {

                        if (response == ButtonType.OK) {

                            topicTableDao.update(topicTable);//อาจแก้

                            checkPk = false;
                        }

                        });

                }else{

                    topicTableDao.save(topicTable);//อาจแก้

                }

                if(!checkPk){
                    FXMLLoader lode = new FXMLLoader(Application.class.getResource("table-property-view.fxml"));
                    Parent  root = null;
                    try {
                        root = lode.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
                    Scene scene = new Scene(root);
                    TablePropertyControl setName = lode.getController();
                    setName.lodeTopicFormTableTopic(tableNameTextField.getText());
                    stage.setScene(scene);
                    stage.show();
                }

//                double height = Screen.getPrimary().getBounds().getHeight()-Screen.getPrimary().getBounds().getHeight()*(20.0/100.0);
//                stage.setHeight(height);
//                stage.setY((Screen.getPrimary().getBounds().getHeight()-stage.getHeight())/2);
//                System.out.println(stage.getX());

        }
    }
    public void cancelButton(ActionEvent event) {
    }


DatabaseConnector databaseConnector;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            DatabaseType mariaDB = new MariaDB(); // obj driver
            mariaDB.getDriverClassName();
            databaseConnector = new DatabaseConnector(mariaDB,"localhost","3306"
                    ,"thena","design_view","1234");
            databaseConnector.getDatabaseConnection(); // บอกให้เชื่อมต่อ

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        this.tableNameTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if(textFieldFunction.checkName(tableNameTextField,newValue,oldValue)){
                    textFieldFunction.removeErrorMassage(messageErrorVbox,errorNameTable);
                }else {
                    int newValueToAscii;
                    for(int charNewValue = 0 ;charNewValue <= newValue.length()-1 ; charNewValue++){
                        newValueToAscii =  newValue.charAt(charNewValue);
                        textFieldFunction.addErrorMassage(messageErrorVbox,errorNameTable,"/"+(char)newValueToAscii+"/  ->Can only put A-Z,a-z,0-9,_");
                    }
                }
                if(textFieldFunction.checkLength(tableNameTextField,16,oldValue)){
                    textFieldFunction.addErrorMassage(messageErrorVbox,errorMaxTable,"Max size "+ tableNameTextField.getLength());
                }else {
                    textFieldFunction.removeErrorMassage(messageErrorVbox,errorMaxTable);
                }
            }
        });
        this.descriptionTextArea.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue) {

                if(textFieldFunction.checkLength(descriptionTextArea,20,oldValue)){
//                    errorTextAreaVbox.getChildren().add(errorNameVbox(errorDescriptionNameTable,"Max size "+ descriptionTextArea.getLength()));
                    textFieldFunction.addErrorMassage(errorTextAreaVbox,errorDescriptionNameTable,"Max size"+ descriptionTextArea.getLength());
                }else{
//                    errorTextAreaVbox.getChildren().remove(errorDescriptionNameTable);
                  textFieldFunction.removeErrorMassage(errorTextAreaVbox,errorDescriptionNameTable);
                }
            }
        });
    }

}
