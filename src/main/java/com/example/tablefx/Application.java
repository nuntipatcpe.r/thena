package com.example.tablefx;

import com.example.tablefx.Model.ColumnType;
import com.example.tablefx.database.*;
import com.example.tablefx.database.DAO.ColumnTypeDAO;
import com.example.tablefx.database.DAO.DAO;
import com.example.tablefx.database.type.DatabaseType;
import com.example.tablefx.database.type.MariaDB;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.*;

public class Application extends javafx.application.Application {
    Stage stage;


    @Override
    public void start(Stage stage) throws Exception {

        try{
            FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("table-topic-view.fxml"));
//          Parent root = FXMLLoader.load(getClass().getResource("tableName-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
//            double height = Screen.getPrimary().getBounds().getHeight()-Screen.getPrimary().getBounds().getHeight()*0.2;
//            double getWidth = Screen.getPrimary().getBounds().getHeight()-Screen.getPrimary().getBounds().getHeight()*0.1;
            double y = primScreenBounds.getHeight()-primScreenBounds.getHeight()*0.2;
            double x =primScreenBounds.getWidth()-primScreenBounds.getWidth()*0.45;

            stage.setHeight(y);
            stage.setWidth(x);

//            stage.setY((Screen.getPrimary().getBounds().getHeight()-stage.getHeight())/2);
//            stage.setX((Screen.getPrimary().getBounds().getWidth()-stage.getWidth())/2);

            stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
            stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);


            stage.setTitle("Table");
            stage.setScene(scene);
            stage.show();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args)  {

        try {

            DatabaseType mariaDB = new MariaDB();
            mariaDB.getDriverClassName();
            DatabaseConnector databaseConnector= new DatabaseConnector(mariaDB,"localhost","3306"
                    ,"thena","design_view","1234");
            databaseConnector.getDatabaseConnection();

            DAO columnTypeDAO = new ColumnTypeDAO(databaseConnector);


//            Customer customer1 = new Customer();
//            customer1.setId("001");
//            customer1.setFirstName("boom");
//            customer1.setLastName("boom2");
//            customer1.setDateOfBirth(new Date());
//            customerDAO.save(customer1);

//            ColumnType columnType1 = new ColumnType();
//            columnType1.setName("gun");
//            columnType1.setDescription(" Gun gonna do the shit RightNow!!");
//            columnType1.setDataType("String");
//
//            columnTypeDAO.delete(columnType1);
//
//            ColumnType columnType2 = new ColumnType();
//            columnType2.setName("gun1");
//            columnType2.setDescription(" Gun gonna do the shit RightNow!!");
//            columnType2.setDataType("String");
           // customerDAO.save(columnType2);
           // columnTypeDAO.save(columnType1);

            List<ColumnType> columnTypeList = columnTypeDAO.getAll();
            List<String> stringList = new ArrayList<>();
            for (ColumnType columnType : columnTypeList ) {
                stringList.add(columnType.getEntity());
            }

            Set<String> set = new HashSet<>(stringList);
            stringList.clear();
            stringList.addAll(set);
            System.out.println(set);




           // List<Customer> customerList = customerDAO.getAll();
//            for (Customer customer : customerList){
//                System.out.printf(customer.getId()+" " + customer.getFirstName());
//                 }

        }catch (Exception e){
            e.printStackTrace();
        }
        launch();


        //create connection for a server installed in localhost, with a user "root" with no password

//        try (Connection conn = DriverManager.getConnection("jdbc:mariadb://localhost/", "root", null)) {
//            // create a Statement
//            try (Statement stmt = conn.createStatement()) {
//                //execute query
//                try (ResultSet rs = stmt.executeQuery("SELECT 'Hello World!'")) {
//                    //position result to first
//                    rs.first();
//                    System.out.println(rs.getString(1)); //result is "Hello World!"
//                }
//            }
//        }
    }
}