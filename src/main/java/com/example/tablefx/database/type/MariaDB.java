package com.example.tablefx.database.type;

import com.example.tablefx.database.type.DatabaseType;

public class MariaDB extends DatabaseType {
    @Override
    public String getName() {
        return "MariaDB";
    }

    @Override
    public String getDriverClassName() {
        return "org.mariadb.jdbc.Driver";
    }

    @Override
    public String getConnectionURLString(String ipAddress, String port, String databaseName) {
        return "jdbc:mariadb://" + ipAddress + ":" + port + "/" + databaseName;
    }

    @Override
    public String getLimitOffsetSQLSyntax(int limitValue, int offsetValue) {
        return "LIMIT " + String.valueOf(limitValue) + " OFFSET " + String.valueOf(offsetValue);
    }

}
