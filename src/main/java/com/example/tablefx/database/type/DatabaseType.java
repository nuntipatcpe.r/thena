package com.example.tablefx.database.type;

public abstract class DatabaseType {
    public abstract String getName();
    public abstract String getDriverClassName();
    public abstract String getConnectionURLString(String ipAddress, String port, String databaseName);
    public abstract String getLimitOffsetSQLSyntax(int limitValue, int offsetValue);
}
