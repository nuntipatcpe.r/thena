package com.example.tablefx.database.DAO;

import com.example.tablefx.database.DatabaseConnector;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class DAO<T> {

    private final DatabaseConnector databaseConnector;

    public DAO(DatabaseConnector databaseConnector){
        this.databaseConnector = databaseConnector;
    }

    protected DatabaseConnector getDatabaseConnector(){
        return this.databaseConnector;
    }

    public T get(Map<String, Object> idArgMap) throws IllegalArgumentException{
        T result = null;
        Connection connection = null;

        boolean allIdAreMatched = true;
        for(String idFieldName : this.getIdFieldNameList()){
            boolean foundMatchingId = false;
            for(String key : idArgMap.keySet()){
                if(key.equals(idFieldName)){
                    foundMatchingId = true;
                    break;
                }
            }

            if(!foundMatchingId){
                allIdAreMatched = false;
                break;
            }
        }

        if(!allIdAreMatched){

            StringBuilder idFieldListStringBuilder = new StringBuilder();
            for(int i=0; i<this.getIdFieldNameList().size(); i++){
                idFieldListStringBuilder.append(idFieldListStringBuilder);
                if( i< (this.getIdFieldNameList().size() - 1) ){
                    idFieldListStringBuilder.append(", ");
                }
            }
            throw new IllegalArgumentException("DAO requires the following ids: " + idFieldListStringBuilder.toString() + " - for processing.");
        }

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(this.getSelectSingleRecordStatement());

            int argumentIndex = 1;
            for(String key : idArgMap.keySet()){
                if(idArgMap.get(key) instanceof String){
                    preparedStatement.setString(argumentIndex, (String) idArgMap.get(key));
                } else if(idArgMap.get(key) instanceof Integer){
                    preparedStatement.setInt(argumentIndex, (Integer) idArgMap.get(key));
                } else if(idArgMap.get(key) instanceof Double){
                    preparedStatement.setDouble(argumentIndex, (Double) idArgMap.get(key));
                } else if(idArgMap.get(key) instanceof java.util.Date){
                    preparedStatement.setDate(argumentIndex, new java.sql.Date( ((java.util.Date)idArgMap.get(key)).getTime() ));
                }

                argumentIndex++;
            }

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result = getObjectFromResultSet(resultSet);
            }

            resultSet.close();
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        } catch (IOException ioException){
            ioException.printStackTrace();
        }

        return result;
    }

    public List<T> getAll(){
        List<T> result = new ArrayList<>();
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            String statementInString = this.getSelectAllRecordStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(statementInString);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                T singleObject = getObjectFromResultSet(resultSet);
                result.add(singleObject);
            }

            resultSet.close();
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        return result;
    }

    public List<T> getAllThatSatisfiesArgument(Map<String, Object> argumentMap){
        List<T> result = new ArrayList<>();
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            StringBuilder statementBuilder = new StringBuilder();
            statementBuilder.append(this.getSelectAllRecordStatement());
            statementBuilder.append(" WHERE ");
            List<String> whereArgList = new ArrayList<>();
            for(String key : argumentMap.keySet()){
                whereArgList.add(key + "=?");
            }

            for(int i=0; i<whereArgList.size(); i++){
                statementBuilder.append(whereArgList.get(i));
                if(i < (whereArgList.size() - 1)){
                    statementBuilder.append(" AND ");
                }
            }
            PreparedStatement preparedStatement = connection.prepareStatement(statementBuilder.toString());

            System.out.println(statementBuilder.toString());

            int argumentIndex = 1;
            for(String key : argumentMap.keySet()){
                System.out.println("argumentIndex: " + argumentIndex + ", key: " + key + ", value: " + argumentMap.get(key));
                if(argumentMap.get(key) instanceof String){
                    preparedStatement.setString(argumentIndex, (String) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Integer){
                    preparedStatement.setInt(argumentIndex, (Integer) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Double){
                    preparedStatement.setDouble(argumentIndex, (Double) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Boolean){
                    preparedStatement.setBoolean(argumentIndex, (Boolean) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof java.util.Date){
                    preparedStatement.setDate(argumentIndex, new java.sql.Date( ((java.util.Date)argumentMap.get(key)).getTime() ));
                }

                argumentIndex++;
            }


            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                T singleObject = getObjectFromResultSet(resultSet);
                result.add(singleObject);
            }

            resultSet.close();
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        return result;
    }

    public int save(T t){
        int rowsAffected = 0;
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            String statementInString = this.getInsertStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(statementInString);

            this.prepareStatementForSaving(t, preparedStatement);

            rowsAffected = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return rowsAffected;
    }

    public int update(T t){
        int rowsAffected = 0;
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(this.getUpdateStatement());
            this.prepareStatementForUpdating(t, preparedStatement);

            rowsAffected = preparedStatement.executeUpdate();

            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return rowsAffected;
    }

    public int updateAllThatSatisfiesArgument(Map<String, Object> argumentMap){

        int rowsAffected = 0;
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            StringBuilder statementBuilder = new StringBuilder();
            statementBuilder.append("UPDATE ");
            statementBuilder.append(this.getTableName());
            statementBuilder.append(" SET ");

            for(int i=0; i<this.getUpdateFieldNameList().size(); i++){
                statementBuilder.append(this.getUpdateFieldNameList().get(i));
                statementBuilder.append("=?");

                if(i<(this.getUpdateFieldNameList().size() - 1)){
                    statementBuilder.append(", ");
                }
            }
            statementBuilder.append(" WHERE ");

            List<String> whereArgList = new ArrayList<>();
            for(String key : argumentMap.keySet()){
                whereArgList.add(key + "=?");
            }

            for(int i=0; i<whereArgList.size(); i++){
                statementBuilder.append(whereArgList.get(i));
                if(i < (whereArgList.size() - 1)){
                    statementBuilder.append(" AND ");
                }
            }
            PreparedStatement preparedStatement = connection.prepareStatement(statementBuilder.toString());

            System.out.println(statementBuilder.toString());

            int argumentIndex = 1;
            for(String key : argumentMap.keySet()){
                System.out.println("argumentIndex: " + argumentIndex + ", key: " + key + ", value: " + argumentMap.get(key));
                if(argumentMap.get(key) instanceof String){
                    preparedStatement.setString(argumentIndex, (String) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Integer){
                    preparedStatement.setInt(argumentIndex, (Integer) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Double){
                    preparedStatement.setDouble(argumentIndex, (Double) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Boolean){
                    preparedStatement.setBoolean(argumentIndex, (Boolean) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof java.util.Date){
                    preparedStatement.setDate(argumentIndex, new java.sql.Date( ((java.util.Date)argumentMap.get(key)).getTime() ));
                }

                argumentIndex++;
            }


            rowsAffected = preparedStatement.executeUpdate();

            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return rowsAffected;
    }

    public int delete(T t){
        int rowsAffected = 0;
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(this.getDeleteStatement());

            this.prepareStatementForDeleting(t, preparedStatement);

            rowsAffected = preparedStatement.executeUpdate();

            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return rowsAffected;
    }

    public int deleteAllThatSatisfiesArgument(Map<String, Object> argumentMap){

        int rowsAffected = 0;
        Connection connection = null;

        try {
            connection = this.getDatabaseConnector().getDatabaseConnection();
            StringBuilder statementBuilder = new StringBuilder();
            statementBuilder.append("DELETE FROM ");
            statementBuilder.append(this.getTableName());
            statementBuilder.append(" WHERE ");

            List<String> whereArgList = new ArrayList<>();
            for(String key : argumentMap.keySet()){
                whereArgList.add(key + "=?");
            }

            for(int i=0; i<whereArgList.size(); i++){
                statementBuilder.append(whereArgList.get(i));
                if(i < (whereArgList.size() - 1)){
                    statementBuilder.append(" AND ");
                }
            }
            PreparedStatement preparedStatement = connection.prepareStatement(statementBuilder.toString());

            System.out.println(statementBuilder.toString());

            int argumentIndex = 1;
            for(String key : argumentMap.keySet()){
                System.out.println("argumentIndex: " + argumentIndex + ", key: " + key + ", value: " + argumentMap.get(key));
                if(argumentMap.get(key) instanceof String){
                    preparedStatement.setString(argumentIndex, (String) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Integer){
                    preparedStatement.setInt(argumentIndex, (Integer) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Double){
                    preparedStatement.setDouble(argumentIndex, (Double) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof Boolean){
                    preparedStatement.setBoolean(argumentIndex, (Boolean) argumentMap.get(key));
                } else if(argumentMap.get(key) instanceof java.util.Date){
                    preparedStatement.setDate(argumentIndex, new java.sql.Date( ((java.util.Date)argumentMap.get(key)).getTime() ));
                }

                argumentIndex++;
            }


            rowsAffected = preparedStatement.executeUpdate();

            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return rowsAffected;
    }

    protected String getSelectAllRecordStatement(){
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append("SELECT ");

        for(int i=0; i<this.getSelectSummaryFieldNameList().size(); i++){
            resultBuilder.append(this.getSelectSummaryFieldNameList().get(i));
            if(i<(this.getSelectSummaryFieldNameList().size()-1)){
                resultBuilder.append(", ");
            }
        }

        resultBuilder.append(" FROM ");
        resultBuilder.append(this.getTableName());

        return resultBuilder.toString();
    }

    protected String getSelectSingleRecordStatement() {
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append(this.getSelectAllRecordStatement());
        resultBuilder.append(" WHERE ");

        for(int i=0; i<this.getIdFieldNameList().size(); i++){
            resultBuilder.append(this.getIdFieldNameList().get(i));
            resultBuilder.append("=?");
            if(i<(this.getIdFieldNameList().size()-1)){
                resultBuilder.append(" AND ");
            }
        }

        return resultBuilder.toString();
    }

    protected String getInsertStatement() {
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append("INSERT INTO ");
        resultBuilder.append(this.getTableName());
        resultBuilder.append("(");

        for(int i=0; i< this.getAllFieldNameList().size(); i++){
            resultBuilder.append(this.getAllFieldNameList().get(i));
            if(i< (this.getAllFieldNameList().size()-1) ){
                resultBuilder.append(", ");
            }
        }
        resultBuilder.append(") VALUES (");
        for(int i=0; i< this.getAllFieldNameList().size(); i++){
            resultBuilder.append("?");
            if(i< (this.getAllFieldNameList().size()-1) ){
                resultBuilder.append(",");
            }
        }
        resultBuilder.append(")");

        return resultBuilder.toString();
    }

    protected String getUpdateStatement() {
        StringBuilder resultBuilder = new StringBuilder();

        resultBuilder.append("UPDATE ");
        resultBuilder.append(this.getTableName());
        resultBuilder.append(" SET ");

        for(int i=0; i<this.getUpdateFieldNameList().size(); i++){
            resultBuilder.append(this.getUpdateFieldNameList().get(i));
            resultBuilder.append("=?");

            if(i<(this.getUpdateFieldNameList().size() - 1)){
                resultBuilder.append(", ");
            }

        }

        resultBuilder.append(" WHERE ");

        for(int i=0; i<this.getIdFieldNameList().size(); i++){
            resultBuilder.append(this.getIdFieldNameList().get(i));
            resultBuilder.append("=?");

            if(i<(this.getIdFieldNameList().size() - 1)){
                resultBuilder.append(" AND ");
            }

        }

        System.out.println(resultBuilder.toString());

        return resultBuilder.toString();
    }

    protected String getDeleteStatement() {
        StringBuilder resultBuilder = new StringBuilder();

        resultBuilder.append("DELETE FROM ");
        resultBuilder.append(this.getTableName());
        resultBuilder.append(" WHERE ");

        for(int i=0; i<this.getIdFieldNameList().size(); i++){
            resultBuilder.append(this.getIdFieldNameList().get(i));
            resultBuilder.append("=?");

            if(i<(this.getIdFieldNameList().size()-1)){
                resultBuilder.append(" AND ");
            }
        }
        return resultBuilder.toString();
    }

    public abstract void prepareStatementForSaving(T t, PreparedStatement preparedStatement) throws SQLException;
    public abstract void prepareStatementForUpdating(T t, PreparedStatement preparedStatement) throws SQLException;
    public abstract void prepareStatementForDeleting(T t, PreparedStatement preparedStatement) throws SQLException;
    public  abstract List<String> getIdFieldNameList();
    public  abstract List<String> getSelectFieldNameList();
    public  abstract List<String> getSelectSummaryFieldNameList();
    public  abstract List<String> getUpdateFieldNameList();
    public  abstract List<String> getAllFieldNameList();
    public  abstract String getTableName();
    public abstract T getObjectFromResultSet(ResultSet resultSet) throws SQLException, IOException;
}
