package com.example.tablefx.database.DAO;

import com.example.tablefx.Model.ColumnType;
import com.example.tablefx.database.DatabaseConnector;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ColumnTypeDAO extends DAO<ColumnType> {

    public ColumnTypeDAO(DatabaseConnector databaseConnector) {
        super(databaseConnector);
    }

    @Override
    public void prepareStatementForSaving(ColumnType columnType, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,columnType.getName());
        preparedStatement.setString(2,columnType.getDescription());
        preparedStatement.setString(3,columnType.getDataType());
        preparedStatement.setString(4, columnType.getKey_name());
        preparedStatement.setString(5, columnType.getIndex_number());
        preparedStatement.setString(6, columnType.getSize());
        preparedStatement.setString(7, columnType.getNot_null());
        preparedStatement.setString(8, columnType.getAdd_auto());
        preparedStatement.setString(9, columnType.getInput_mask());
        preparedStatement.setString(10, columnType.getFormat());
        preparedStatement.setString(11, columnType.getDecimal_number());
        preparedStatement.setString(12, columnType.getDefault_value());
        preparedStatement.setString(13, columnType.getValidation_text());
        preparedStatement.setString(14, columnType.getRegex());
        preparedStatement.setString(15, columnType.getEntity());
    }

    @Override
    public void prepareStatementForUpdating(ColumnType columnType, PreparedStatement preparedStatement) throws SQLException {

        preparedStatement.setString(1,columnType.getDescription());
        preparedStatement.setString(2,columnType.getDataType());
        preparedStatement.setString(3, columnType.getKey_name());
        preparedStatement.setString(4, columnType.getIndex_number());
        preparedStatement.setString(5, columnType.getSize());
        preparedStatement.setString(6, columnType.getNot_null());
        preparedStatement.setString(7, columnType.getAdd_auto());
        preparedStatement.setString(8, columnType.getInput_mask());
        preparedStatement.setString(9, columnType.getFormat());
        preparedStatement.setString(10, columnType.getDecimal_number());
        preparedStatement.setString(11, columnType.getDefault_value());
        preparedStatement.setString(12, columnType.getValidation_text());
        preparedStatement.setString(13, columnType.getRegex());

        preparedStatement.setString(14,columnType.getName());
        preparedStatement.setString(15,columnType.getEntity());

    }

    @Override
    public void prepareStatementForDeleting(ColumnType columnType, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,columnType.getName());
        preparedStatement.setString(2,columnType.getEntity());

    }

    @Override
    public List<String> getIdFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("entity");
        return stringList;
    }

    @Override
    public List<String> getSelectFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        stringList.add("dataType");
        stringList.add("key_name");
        stringList.add("index_number");
        stringList.add("size");
        stringList.add("not_null");
        stringList.add("add_auto");
        stringList.add("input_mask");
        stringList.add("format");
        stringList.add("decimal_number");
        stringList.add("default_value");
        stringList.add("validation_text");
        stringList.add("regex");
        stringList.add("entity");
        return stringList;
    }

    @Override
    public List<String> getSelectSummaryFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        stringList.add("dataType");
        stringList.add("key_name");
        stringList.add("index_number");
        stringList.add("size");
        stringList.add("not_null");
        stringList.add("add_auto");
        stringList.add("input_mask");
        stringList.add("format");
        stringList.add("decimal_number");
        stringList.add("default_value");
        stringList.add("validation_text");
        stringList.add("regex");
        stringList.add("entity");
        return stringList;
    }

    @Override
    public List<String> getUpdateFieldNameList() {//ห้ามAddPk
        List<String> stringList = new ArrayList<>();
        stringList.add("description");
        stringList.add("dataType");
        stringList.add("key_name");
        stringList.add("index_number");
        stringList.add("size");
        stringList.add("not_null");
        stringList.add("add_auto");
        stringList.add("input_mask");
        stringList.add("format");
        stringList.add("decimal_number");
        stringList.add("default_value");
        stringList.add("validation_text");
        stringList.add("regex");
        return stringList;
    }

    @Override
    public List<String> getAllFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        stringList.add("dataType");
        stringList.add("key_name");
        stringList.add("index_number");
        stringList.add("size");
        stringList.add("not_null");
        stringList.add("add_auto");
        stringList.add("input_mask");
        stringList.add("format");
        stringList.add("decimal_number");
        stringList.add("default_value");
        stringList.add("validation_text");
        stringList.add("regex");
        stringList.add("entity");
        return stringList;
    }

    @Override
    public String getTableName() {
        String name = "column_type"; //ชื่อต้องตรงกับ DB for sure
        return name;
    }

    @Override
    public ColumnType getObjectFromResultSet(ResultSet resultSet) throws SQLException, IOException {
        ColumnType columnType = new ColumnType();
        columnType.setName(resultSet.getString(1));
        columnType.setDescription(resultSet.getString(2));
        columnType.setDataType(resultSet.getString(3));
        columnType.setKey_name(resultSet.getString(4));
        columnType.setIndex_number(resultSet.getString(5));
        columnType.setSize(resultSet.getString(6));
        columnType.setNot_null(resultSet.getString(7));
        columnType.setAdd_auto(resultSet.getString(8));
        columnType.setInput_mask(resultSet.getString(9));
        columnType.setFormat(resultSet.getString(10));
        columnType.setDecimal_number(resultSet.getString(11));
        columnType.setDefault_value(resultSet.getString(12));
        columnType.setValidation_text(resultSet.getString(13));
        columnType.setRegex(resultSet.getString(14));
        columnType.setEntity(resultSet.getString(15));
        return columnType;
    }
}
