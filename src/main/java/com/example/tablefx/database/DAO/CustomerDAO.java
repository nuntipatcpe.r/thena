package com.example.tablefx.database.DAO;

import com.example.tablefx.Model.Customer;
import com.example.tablefx.database.DAO.DAO;
import com.example.tablefx.database.DatabaseConnector;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO extends DAO<Customer> {

    public CustomerDAO(DatabaseConnector databaseConnector) {
        super(databaseConnector);
    }
    @Override
    public void prepareStatementForSaving(Customer customer, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,customer.getId());
        preparedStatement.setString(2,customer.getFirstName());
        preparedStatement.setString(3,customer.getLastName());
        preparedStatement.setDate(4,new java.sql.Date(customer.getDateOfBirth().getTime()));
    }

    @Override
    public void prepareStatementForUpdating(Customer customer, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,customer.getFirstName());
        preparedStatement.setString(2,customer.getLastName());
        preparedStatement.setDate(3,new java.sql.Date(customer.getDateOfBirth().getTime()));

        preparedStatement.setString(4,customer.getId());//pk
    }

    @Override
    public void prepareStatementForDeleting(Customer customer, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,customer.getId());
    }

    @Override
    public List<String> getIdFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("id");
        return stringList;
    }

    @Override
    public List<String> getSelectFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("id");
        stringList.add("firstname");
        stringList.add("lastname");
        stringList.add("dateofbirth");
        return stringList;
    }

    @Override
    public List<String> getSelectSummaryFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("id");
        stringList.add("firstname");
        stringList.add("lastname");
        stringList.add("dateofbirth");
        return stringList;
    }

    @Override
    public List<String> getUpdateFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("firstname");
        stringList.add("lastname");
        stringList.add("dateofbirth");
        return stringList;
    }

    @Override
    public List<String> getAllFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("id");
        stringList.add("firstname");
        stringList.add("lastname");
        stringList.add("dateofbirth");
        return stringList;
    }

    @Override
    public String getTableName() {
        String name = "customer";
        return name;
    }

    @Override
    public Customer getObjectFromResultSet(ResultSet resultSet) throws SQLException, IOException {
        Customer customer = new Customer();
        customer.setId(resultSet.getString(1));
        customer.setFirstName(resultSet.getString(2));
        customer.setLastName(resultSet.getString(3));
        customer.setDateOfBirth(resultSet.getDate(4));
        return customer;
    }
}
