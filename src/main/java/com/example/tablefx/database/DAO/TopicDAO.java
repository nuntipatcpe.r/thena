package com.example.tablefx.database.DAO;

import com.example.tablefx.Model.Topic;
import com.example.tablefx.database.DatabaseConnector;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TopicDAO extends  DAO<Topic>{
    public TopicDAO(DatabaseConnector databaseConnector) {
        super(databaseConnector);
    }

    @Override
    public void prepareStatementForSaving(Topic topicTable, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,topicTable.getName());
        preparedStatement.setString(2,topicTable.getDescription());

    }

    @Override
    public void prepareStatementForUpdating(Topic topicTable, PreparedStatement preparedStatement) throws SQLException {

        preparedStatement.setString(1,topicTable.getDescription());
        preparedStatement.setString(2,topicTable.getName());
    }

    @Override
    public void prepareStatementForDeleting(Topic topicTable, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1,topicTable.getName());
    }

    @Override
    public List<String> getIdFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        return stringList;
    }

    @Override
    public List<String> getSelectFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        return stringList;
    }

    @Override
    public List<String> getSelectSummaryFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        return stringList;
    }

    @Override
    public List<String> getUpdateFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("description");
        return stringList;
    }

    @Override
    public List<String> getAllFieldNameList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("name");
        stringList.add("description");
        return stringList;
    }

    @Override
    public String getTableName() {
        String name = "topic";
        return name;
    }

    @Override
    public Topic getObjectFromResultSet(ResultSet resultSet) throws SQLException, IOException {
        Topic topicTable = new Topic();
        topicTable.setName(resultSet.getString(1));
        topicTable.setDescription(resultSet.getString(2));
        return topicTable;
    }
}
