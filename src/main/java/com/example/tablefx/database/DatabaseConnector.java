package com.example.tablefx.database;


import com.example.tablefx.database.type.DatabaseType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {
    private final DatabaseType databaseType;
    private final String ipAddress;
    private final String port;
    private final String databaseName;
    private final String databaseUsername;
    private final String databasePassword;


    public DatabaseConnector(DatabaseType databaseType, String ipAddress, String port, String databaseName, String databaseUsername, String databasePassword){
        this.databaseType = databaseType;
        this.ipAddress = ipAddress;
        this.port = port;
        this.databaseName = databaseName;
        this.databaseUsername = databaseUsername;
        this.databasePassword = databasePassword;
    }

    public Connection getDatabaseConnection() throws SQLException, ClassNotFoundException {
        Class.forName(this.databaseType.getDriverClassName());
        return DriverManager.getConnection(
                this.databaseType.getConnectionURLString(this.ipAddress, this.port, this.databaseName),
                databaseUsername,
                databasePassword);
    }

    public DatabaseType getDatabaseType() {
        return this.databaseType;
    }
}
