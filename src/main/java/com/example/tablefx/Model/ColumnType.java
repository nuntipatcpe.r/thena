package com.example.tablefx.Model;

//        name varchar(32) NOT NULL,
//        description varchar(254),
//        dataType varchar(254),
//        key_name varchar(254),
//        index_number varchar(254),
//        size varchar(254),
//        not_null varchar(10),
//        add_auto varchar(10),
//        input_mask varchar(254),
//        format varchar(254),
//        decimal_number varchar(254),
//        default_value varchar(254),
//        validation_text varchar(254),
//        regex  varchar(254),
//        PRIMARY KEY (name));
public class ColumnType {

    private String name;
    private String description;
    private String dataType;
    private String key_name;
    private String index_number;
    private String size;
    private String not_null;
    private String add_auto;
    private String input_mask;
    private String format;
    private String decimal_number;
    private String default_value;
    private String validation_text;
    private String regex;
    private String entity;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getKey_name() {
        return key_name;
    }

    public void setKey_name(String key_name) {
        this.key_name = key_name;
    }

    public String getIndex_number() {
        return index_number;
    }

    public void setIndex_number(String index_number) {
        this.index_number = index_number;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getNot_null() {
        return not_null;
    }

    public void setNot_null(String not_null) {
        this.not_null = not_null;
    }

    public String getAdd_auto() {
        return add_auto;
    }

    public void setAdd_auto(String add_auto) {
        this.add_auto = add_auto;
    }

    public String getInput_mask() {
        return input_mask;
    }

    public void setInput_mask(String input_mask) {
        this.input_mask = input_mask;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDecimal_number() {
        return decimal_number;
    }

    public void setDecimal_number(String decimal_number) {
        this.decimal_number = decimal_number;
    }

    public String getDefault_value() {
        return default_value;
    }

    public void setDefault_value(String default_value) {
        this.default_value = default_value;
    }

    public String getValidation_text() {
        return validation_text;
    }

    public void setValidation_text(String validation_text) {
        this.validation_text = validation_text;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
