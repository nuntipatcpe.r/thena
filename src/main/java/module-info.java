
module com.example.tablefx {

    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires java.sql;

    opens com.example.tablefx to javafx.fxml;
    exports com.example.tablefx;
    exports com.example.tablefx.Controler;
    opens com.example.tablefx.Controler to javafx.fxml;
    exports com.example.tablefx.Controler.FunctionTogether;
    opens com.example.tablefx.Controler.FunctionTogether to javafx.fxml;
    exports com.example.tablefx.database;
    opens com.example.tablefx.database to javafx.fxml;
    exports com.example.tablefx.database.type;
    opens com.example.tablefx.database.type to javafx.fxml;
    exports com.example.tablefx.database.DAO;
    opens com.example.tablefx.database.DAO to javafx.fxml;

}